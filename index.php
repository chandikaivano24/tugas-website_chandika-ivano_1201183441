<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="index.css">
    <title>iTECHNEWS</title>
  </head>
  <body data-spy='scroll' data-target='.navbar' data-offset='50'>

  <!-- My CSS -->
  <style>
    section {
        min-height = 50px;
    }
    .logo {
        opacity: 100 !important;
    }
  </style>

    <!--navbar-->

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
        <a class="navbar-brand" href="#navbar">iTECHNEWS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link active" href="#navbar">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="#news">News</a>
            <a class="nav-item nav-link" href="#contact">Contact</a>
            </div>
        </div>
        </div>
    </nav>

    <!--jumbotron-->   

    <div class="jumbotron jumbotron-fluid" id="navbar">
    <div class="container text-center">
        <img src="">
        <h1 class="display-4">Chandika Ivano</h1>
        <p class="lead">Welcome to iTECHNEWS. <br> We will talk about technology and any other incredible thing all around the world.</p>
    </div>
    </div>

    <!-- News -->
    <div class="container" id="news">
    <div class="container">
        <div class="row pb-5">
            <div class="col">
                <h1>News</h1>
            </div>
        </div>

        <div class="row text-justify">
            <div class="col pb-4">
                <p>
                YOLO atau You Only Look Once adalah sebuah pendekatan baru untuk sistem pendeteksian objek, yang ditargetkan untuk pemrosesan secara real-time. YOLO membingkai pendeteksian objek sebagai masalah regresi tunggal, dimana dari piksel gambar langsung ke kotak pembatas (bounding box) spasial yang terpisah dan probabilitas kelas yang terkait. YOLO melakukan pendeteksian dan pengenalan objek dengan sebuah jaringan syaraf tunggal (single neural network), yang memprediksi kotak-kotak pembatas dan probabilitas kelas secara langsung dalam satu evaluasi (Redmon et al., 2015). <br>
                YOLO sendiri pertama kali diciptakan pada tahun 2015 oleh Joseph Redmon. Awalnya YOLO akan menangkap berbagai kemungkinan objek yang terdeteksi kedalam sebuah kotak, kemudian akan melakukan selecting terhadap kotak yang memiliki tingkat kepercayaan yang disesuaikan oleh user tergantung dengan kebutuhannya. Barulah akan didapatkan objek yang sesuai dengan hasil terbaik. <br>
                Hingga saat ini YOLO banyak digunakan pada kamera pengintai atau CCTV pada fasilitas umum seperti ATM, kamera dilampu lalu lintas, perkantoran dan lain sebagainya.
                <br>

                Konsep dasar dari YOLO adalah sistem dibuat dengan mengumpulkan dataset dari seluruh obyek yang ingin dikenali. Hal ini bertujuan agar YOLO mengenali dan dapat menganalisis obyek-obyek yang ada. Dengan begitu YOLO dapat membentuk frame untuk setiap obyek.
                <br>
                Selanjutnya YOLO diberikan training. Training sendiri bertujuan agar obyek yang ditentukan sebelumnya pada dataset dapat dikenali dengan baik. Hal ini biasanya diukur dari jumlah step. Hal lain yang dijadikan indikasi keberhasilan YOLO adalah akurasi berdasarkan jumlah obyek yang terdeteksi. Pengujian ini dapat dianalisis dari hasil obyek yang terdeteksi oleh YOLO dan kemudian dibandingkan dengan obyek yang sesungguhnya.
                <br>
                Terkahir adalah implementasi YOLO pada kamera pengamat yang ada dilingkungan. Seperti terdapat pada CCTV, ATM dan lain-lain.
                <br>
                YOLO sendiri biasanya digunkan dengan mengintegrasikan sistem-sistem sederhana yang ada dan kemudian menjadikannya suatu sistem yang lebih advance dalam pengaplikasiannya. Seperti mengintegrasikan YOLO pada kamera CCTV dan kemudian dapat mematikan rangkaian listrik jika tidak ditemukan adanya manusia yang ada dalam suatu ruangan.
                <br>
                <br>
                <br>
                <br>
                Referensi
                <br>
                <br>
                <br>
                Abi Rachman Wasil, G. M. (2019). Pembuatan Pendeteksi Obyek Dengan Metode You Only Look Once (YOLO) Untuk Automated Teller Machine (ATM).
                <br>
                <br>
                Fathoni Debri Hasbi. (2019). RANCANG BANGUN SISTEM SMART CCTV UNTUK EFEKTIVITAS ENERGI BERBASIS YOLO CNN DAN ANDROID DI LABORATORIUM OTOMASI PPNS.
               </p>
            </div>
        </div>
    </div>
    </div>

    <!-- Contact -->
    <section id="contact" class="contact bg-light">
        <div class="container bg-light" >
            <div class="row pb-3 pt-3">
                <div class="col">
                    <h1>Find Me</h1>
                </div>
            </div>
            <div class="container col pb-5">
                <div class="row pt-3">
                    <div class="col-sm">
                    <a href="https://www.instagram.com/chandikaivano24/">Instagram</a>
                    </div>
                    <div class="col-sm">
                    <a href="https://www.facebook.com/chandika.ivanno">Facebook</a>
                    </div>
                    <div class="col-sm">
                    <a href="mailto:chandikaivano24@gmail.com">email</a>
                    </div>
                </div>
            </div>
            </div>
            </div> 
        </div>
    </section>

    <!--copyright-->
    <footer class="bg-dark text-white">
        <div class="container">
            <div class="row text-center pt-3">
                <div class="col">
                    <p>Copyright 2020 iTECHNEWS</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>